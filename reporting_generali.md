---
title: "K1 Signal Performance Report"
subtitle: "Banca Generali"
author: ""
date: "2016-12-01 12:17:55"
output:
  prettydoc::html_pretty:
    theme: cayman
    highlight: github
  html_document:
    fig_caption: yes
    keep_md: yes
    self_contained: Yes
---
 ![](./42cxlogo.png)

### Trading Period
| First Date | Last Date | Trading Days|
|:-------------:|:-----------:|:-----------:|
| 2016-11-29| 2016-11-30| 2|

# **Equity Line (Initial Investment 25 Mio USD)**[^1]
<img src="figure/QBG4-1.png" title="plot of chunk QBG4" alt="plot of chunk QBG4" style="display: block; margin: auto;" />

# **Total Equity (in Mio USD)**
| Date |  | Equity|
|:-------------:|:-----------:|:-----------:|
| Start (2016-11-29)| | 25|
| Current (2016-11-30)| | 24.89

# **PnL**

|     | 10 days|  30 days |Total|YM/Close| 
|------------------------------|:---------------:|:--------:|:--------:|:------:|
| **PnL (in Mio USD)**       |    **-0.12**|**-0.12** | **-0.12**|00.06|
| **PnL (in %)**       |    **-0.46**|**-0.46** | **-0.46**|00.24|

# **Risk and Performance Diagnostics**

|Risk/Cost Metrics      | 10 days|  30 days |Total|YM/Close| 
|------------------------------|:---------------:|:--------:|:--------:|:------:|
| Avg. Trade Profit (in bps) | -22.94|-22.94|-22.94|11.79|
| Signal Accuracy (in %)                  | 00.00 |00.00|00.00|100.00|
| Annualized Sharpe  Ratio                   | -50.09  |-50.09  | -50.09|26.70|
| **Max. Drawdown (MDD) (in %)**                | **00.28** |**00.28** |**00.28**|00.00|
| PnL/MDD  | 01.63|01.63|01.63|  Inf|
| Round Trip Costs (in bps)| 01.00|01.00 |01.00|`--`|


# **Trade Diagnostics**

| Long/Short Trades      |       | Win/Loss Trades  || Trade Profits (in bps)  ||
| ------------- |-------------:| -----:|-----:|-----:|----:|
| All     |  2       |All   |    2 |  Avg | -22.94|
| Long      | 1|  Win Trades    |   0 | Max Profit | -17.77|
| Short | 1 | Loss Trades      |    2 | Max  Loss | 028.11|

[^1]: Left panel shows the equity line. Right panel shows the rolling Sharpe ratio, the rolling maximum drawdown (MDD) and the last trade performance. The lower panel shows the daily strategy returns in basis points. 



