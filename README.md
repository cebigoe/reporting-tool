# Reporting README
Author: Dr. Gökhan Cebiroglu

Several Steps to ensure that the reporting tool works properly

1.  Install R
* Go to https://cran.r-project.org/bin/windows/base/
* and install the latest version of R (R.3.3.2)

2. Install RStudio
* Go to https://www.rstudio.com/products/rstudio/download/
* and install RStudio

3. Install packages
* Open RStudio
* Go to the lower right panel and click on "Packages"
 * then click on "Install"
 * Install the following packages: curl, jsonlite, zoo, markdown, knitr
 * I have built a work-around that automatically fetches these package, but to be 100 % sure it is better to install them directly
 - Please report if installation of packages worked

4. Assoaciate Rexecutables with Windows
 - tba

5. Configuration and System Set up
 - tba