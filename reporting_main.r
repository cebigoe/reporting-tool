## @knitr Q2 --------
packages<-c("jsonlite","zoo","markdown","knitr","prettydoc","rmarkdown")
inst_packs<-installed.packages()[,1]
toinst<-packs<-setdiff(packages,inst_packs)
for(pack in toinst)
{
  install.packages(pack)
}
for(pack in packages)
{
  require(pack,character.only=TRUE)
}

library(prettydoc)
library(rmarkdown)
library(knitr)
library(markdown)

reportingurl<-"http://signal.ws.unosoft.at/FinWS.svc/LIVE_SIGNALS_VIEW?$format=json"
reporting_horizon<-NULL
signaldataraw<-fromJSON(txt=reportingurl)
signaldata<-signaldataraw$value
signaldata<-signaldata[order(signaldata$SignalDate),]
signaldata<-signaldata[complete.cases(signaldata$ReturnBPS),]


if(!is.null(reporting_horizon))
{
  idx<-seq(1,reporting_horzon)
  signaldata<-signaldata[idx,]
}




## @knitr Q3 --------
tcost<-10^(-4)
rets<-signaldata$ReturnBPS
close<-c(signaldata$OpenPrice[1],signaldata$ClosePrice)
rets_ym<-diff(close)/close[-length(close)]
rets_ym<-ifelse(is.na(rets_ym),0,rets_ym)
equity_ym<-(cumprod(rets_ym+1)-1)*100
preds_ym<-rep(1,length(rets_ym))
preds<-signaldata$AdjustedPrediction
trpos<-rets*preds-tcost
trpos<-ifelse(is.na(trpos),0,trpos)
trpos<-ifelse(preds==0,0,trpos)
modelname<-signaldata$Model
title<-signaldata$Title
date<-signaldata$SignalDate
date_begin<-strsplit(min(date),split="T")[[1]][1]
date_end<-strsplit(max(date),split="T")[[1]][1]
ntrades<-length(which(preds!=0))
equity<-cumprod(trpos+1)
stratreport<-get_strategy_report(preds,trpos)
stratreport_ym<-get_strategy_report(preds_ym,rets_ym)
s<-data.frame(stratreport)
sym<-data.frame(stratreport_ym)
s0<-round(data.frame(t(s)),2)
s0ym<-round(data.frame(t(sym)),2)

delta<-10
trpos10<-tail(trpos,delta)
preds10<-tail(preds,delta)
stratreport10<-get_strategy_report(preds10,trpos10)
s10<-data.frame(stratreport10)
s10<-round(data.frame(t(s10)),2)

delta<-30
trpos30<-tail(trpos,delta)
preds30<-tail(preds,delta)
stratreport30<-get_strategy_report(preds30,trpos30)
s30<-data.frame(stratreport30)
s30<-round(data.frame(t(s30)),2)









