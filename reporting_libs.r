## @knitr Q1 --------

sharpe<-function(trpos)
{
  mean(trpos,na.rm=TRUE)/sd(trpos,na.rm=TRUE)*sqrt(255)
  
}

report.performance<-function(trpos)
{
  
  equity<-(cumprod(trpos+1)-1)*100
  tailperformance<-tail(equity,n=1)
  tailperformance
}





report.maxdrawdown<-function(trpos)
{
  equity_base<-cumprod(trpos+1)
  maxdrawdown<-cummax(equity_base)-equity_base
  maxdrawdown<-maxdrawdown/cummax(equity_base)
  maxdrawdown<--100*maxdrawdown
  min(maxdrawdown,na.rm=TRUE)
  
}

report.maxdrawdown_profile<-function(trpos)
{
  equity_base<-cumprod(trpos+1)
  maxdrawdown<-cummax(equity_base)-equity_base
  maxdrawdown<-maxdrawdown/cummax(equity_base)
  maxdrawdown<--100*maxdrawdown
  maxdrawdown
}

report.sharpe<-function(trpos)
{
  mean(trpos,na.rm=TRUE)/sd(trpos,na.rm=TRUE)*sqrt(255)
}

report.ntrades<-function(pos,trpos)
{
  idx<-which(pos!=0)
  length(idx)
  
}

report.PnlMddRatio<-function(pos,trpos)
{
  
  abs(report.performance(trpos)/report.maxdrawdown(trpos))
}

report.nlongtrades<-function(pos,trpos)
{
  idx<-which(pos>0)
  length(idx)
  
}

report.nshorttrades<-function(pos,trpos){
  
  idx<-which(pos<0)
  length(idx)
  
}

report.avgtradeprofit<-function(pos,trpos)
{
  idx<-which(pos!=0)
  mean(trpos[idx],na.rm=TRUE)*10000
}

report.avgwintrade<-function(pos,trpos){
  
  idx<-which(trpos>0)
  mean(trpos[idx],na.rm=TRUE)*10000
}


report.avglosstrade<-function(pos,trpos){
  
  idx<-which(trpos<0)
  mean(trpos[idx],na.rm=TRUE)*10000
}

report.maxwintrade<-function(pos,trpos)
{
  max(trpos,na.rm=TRUE)*10000
  
}

report.minlosstrade<-function(pos,trpos)
{
  min(trpos)*10000
  
}

report.nwintrades<-function(pos,trpos)
{ 
  idx<-which(pos!=0)
  trpos<-trpos[idx]
  length(which(trpos>0))
}

report.nlostrades<-function(pos,trpos)
{
  idx<-which(pos!=0)
  trpos<-trpos[idx]
  length(which(trpos<0))
}

report.accuracy<-function(pos,trpos)
{
  
  report.nwintrades(pos,trpos)/report.ntrades(pos,trpos)*100
}

get_strategy_report<-function(pos,trpos)
{
  perf<-report.performance(trpos)
  sharp<-report.sharpe(trpos)
  maxdrwdwn<-report.maxdrawdown(trpos)
  ntrades<-report.ntrades(pos,trpos)
  nlong<-report.nlongtrades(pos,trpos)
  nshort<-report.nshorttrades(pos,trpos)
  ntotal<-nlong+nshort
  nwin<-report.nwintrades(pos,trpos)
  nlos<-report.nlostrades(pos,trpos)
  accr<-report.accuracy(pos,trpos)
  avg_trprofit<-report.avgtradeprofit(pos,trpos)
  avg_wintrade<-report.avgwintrade(pos,trpos)
  avg_losstrade<-report.avglosstrade(pos,trpos)
  max_wintrade<-report.maxwintrade(pos,trpos)
  min_losstrade<-report.minlosstrade(pos,trpos)
  pnlmddratio<-report.PnlMddRatio(pos,trpos)
  
  stats<-c(performance=perf,
           sharpe=sharp,
           maxdrwdwn=maxdrwdwn,
           ntrades=ntrades,
           nlong=nlong,
           nshort=nshort,
           ntotal=ntotal,
           nwin=nwin,
           nlos=nlos,
           accr=accr,
           avg_trprofit=avg_trprofit,
           avg_wintrade=avg_wintrade,
           avg_losstrade=avg_losstrade,
           max_wintrade=max_wintrade,
           min_losstrade=min_losstrade,
           pnlmddratio=pnlmddratio)
  stats
  
}


## String Split for dates




get_date<-function(datestring)
{
  datepart<-strsplit(datestring,split="T")[[1]][1]
  datepart
}

get_dates<-function(datestring_vec)
{
  val<-sapply(datestring_vec,FUN = get_date)
  val<-as.vector(unlist(val))
  val
}

formN<-function(num,d=5){
  formatC(round(num, 2), big.mark=",", format="f",digits=2,width=d,flag="0")
  #sprintf("% 5.2f",round(num,2))
  #format(round(num,2),digits=2,nsmall=2,width=5,)
}


##### Plotting functions

plot_equity<-function(portfolio_return,benchmark_return,names,window,filename="equityline.png")
{
  
  width<-800
  height<-500
  units<-"px"
  
  path<-"/Users/cebigoe/Dropbox (Privat)/Consulting/42cx/multimodel/tyndaris/"
  filename<-file.path(path,filename)
  library("zoo")
  ylab="PnL (in %)"
  main="PnL"
  
  
  
  vals<-c(rollsharpe_portf,rollsharpe_benchmark)
  maxval<-max(vals)
  minval<-min(vals)
  
  png(width=width,height=height,units=units,filename=filename)
  
  plot(rollsharpe_portf,type="l",lwd=3,col="blue",xlab=xlab,ylab=ylab,main=main,xaxt="n",ylim=c(minval,maxval))
  points(rollsharpe_benchmark,type="l",lwd=3,col="green")
  legendname1<-names[1]
  legendname2<-names[2]
  legendnames<-c(names[1],names[2])
  legendcols<-c("blue","green")
  legend("bottomright",legend = legendnames,col=legendcols,lty=1,lwd=2,x.intersp = 2)
  n<-length(rollsharpe_portf)
  delta<-floor(n/15)
  abline(h=0,lwd=1,col="gray")
  datepoints<-seq(1,length(rollsharpe_portf),delta)
  axis(1,at=datepoints,dates1[datepoints+window])
  Sys.sleep(0.5)
  graphics.off()
  
}


plot_rollsharpe<-function(portfolio_return,benchmark_return,names,window,filename="maxdrawdown.png")
{
  
  width<-800
  height<-500
  units<-"px"
  
  
  path<-"/Users/cebigoe/Dropbox (Privat)/Consulting/42cx/multimodel/tyndaris"
  filename<-file.path(path,filename)
  library("zoo")
  ylab="Rolling Sharpe"
  main="Rolling Sharpe (50 Days)"
  
  ### Rolling Sharpe
  rollsharpe_portf<-rollapply(portfolio_return,FUN=report.sharpe,window)
  rollsharpe_benchmark<-rollapply(benchmark_return,FUN=report.sharpe,window)
  
  vals<-c(rollsharpe_portf,rollsharpe_benchmark)
  maxval<-max(vals,na.rm=TRUE)
  minval<-min(vals,na.rm=TRUE)
  
  png(width=width,height=height,units=units,filename=filename)
  
  plot(rollsharpe_portf,type="l",lwd=3,col="blue",xlab=xlab,ylab=ylab,main=main,xaxt="n",ylim=c(minval,maxval))
  points(rollsharpe_benchmark,type="l",lwd=3,col="green")
  legendname1<-names[1]
  legendname2<-names[2]
  legendnames<-c(names[1],names[2])
  legendcols<-c("blue","green")
  legend("bottomright",legend = legendnames,col=legendcols,lty=1,lwd=2,x.intersp = 2)
  n<-length(rollsharpe_portf)
  delta<-floor(n/15)
  abline(h=0,lwd=1,col="gray")
  datepoints<-seq(1,length(rollsharpe_portf),delta)
  axis(1,at=datepoints,dates1[datepoints+window])
  Sys.sleep(0.5)
  graphics.off()
  
  
}

plot_maxdrawdwn<-function(portfolio_return,benchmark_return,names,filename="maxdrawdwn.png")
{
  
  width<-800
  height<-500
  units<-"px"
  
  path<-"/Users/cebigoe/Dropbox (Privat)/Consulting/42cx/multimodel/tyndaris"
  filename<-file.path(path,filename)
  library("zoo")
  ylab="MaxDrawDown (in %)"
  xlab=""
  main="MaxDrawDown"
  
  maxdrawdown_portf<-report.maxdrawdown_profile(portfolio_return)
  maxdrawdown_benchmark<-report.maxdrawdown_profile(benchmark_return)
  
  
  vals<-c(maxdrawdown_portf,maxdrawdown_benchmark)
  maxval<-max(vals,na.rm=TRUE)
  minval<-min(vals,na.rm=TRUE)
  
  
  png(width=width,height=height,units=units,filename=filename)
  
  plot(maxdrawdown_portf,type="l",col="blue",lwd=3,xaxt="n",xlab=xlab,ylab=ylab,ylim=c(minval,maxval),main=main)
  points(maxdrawdown_benchmark,type="l",col="green",lwd=3)
  m<-length(dates1)
  delta<-floor(m/15)
  datepoints<-seq(1,m,delta)
  axis(1,at=datepoints,labels=dates1[datepoints])
  legendcols<-c("blue","green")
  legend("bottomright",legend = legendnames,col=legendcols,lty=1,lwd=3,x.intersp = 2)
  Sys.sleep(0.5)
  graphics.off()
}

plot_rollvola<-function(portfolio_return,benchmark_return,names,window,filename="maxdrawdown.png")
{
  
  width<-800
  height<-500
  units<-"px"
  
  
  path<-"/Users/cebigoe/Dropbox (Privat)/Consulting/42cx/multimodel/tyndaris"
  filename<-file.path(path,filename)
  library("zoo")
  ylab="Ann. Standard Deviation (in %)"
  main="Annualized Rolling Volatility (rolling 50 Days)"
  
  ### Rolling Sharpe
  rollvola_portf<-rollapply(portfolio_return,FUN=sd,window)*100*sqrt(255)
  rollvola_benchmark<-rollapply(benchmark_return,FUN=sd,window)*100*sqrt(255)
  
  vals<-c(rollvola_portf,rollvola_benchmark)
  maxval<-max(vals,na.rm=TRUE)
  minval<-min(vals,na.rm=TRUE)
  
  png(width=width,height=height,units=units,filename=filename)
  
  plot(rollvola_portf,type="l",lwd=3,col="blue",xlab=xlab,ylab=ylab,main=main,xaxt="n",ylim=c(minval,maxval))
  points(rollvola_benchmark,type="l",lwd=3,col="green")
  legendname1<-names[1]
  legendname2<-names[2]
  legendnames<-c(names[1],names[2])
  legendcols<-c("blue","green")
  legend("bottomright",legend = legendnames,col=legendcols,lty=1,lwd=2,x.intersp = 2)
  n<-length(rollvola_portf)
  delta<-floor(n/15)
  abline(h=0,lwd=1,col="gray")
  datepoints<-seq(1,length(rollvola_portf),delta)
  axis(1,at=datepoints,dates1[datepoints+window])
  Sys.sleep(0.5)
  graphics.off()
  
  
}


plot_equity<-function()
{
  
}


