---
title: "K1 Signal Performance Report"
subtitle: "Banca Generali"
author: ""
date: "`r Sys.time()`"
output:
  prettydoc::html_pretty:
    theme: cayman
    highlight: github
  html_document:
    fig_caption: yes
    keep_md: yes
    self_contained: Yes
---
 ![](./42cxlogo.png)
```{r QBG1,echo=FALSE,cache=FALSE,warning=FALSE,include=FALSE}
source("reporting_libs.r")
source("reporting_main_generali.r")
```
### Trading Period
| First Date | Last Date | Trading Days|
|:-------------:|:-----------:|:-----------:|
| `r date_begin`| `r date_end`| `r ntrades`|

# **Equity Line (Initial Investment 25 Mio USD)**[^1]
```{r QBG4,echo=FALSE,cache=FALSE,fig.align='center',fig.width=10,fig.height=4,dpi=100,autodep=TRUE}

tcost<-10^(-4)
rets<-signaldata$ReturnBPS
preds<-signaldata$AdjustedPrediction
trpos<-rets*preds-tcost
ldayreturn<-sprintf("%+.3f",tail(round(100*trpos,3),1))
ldayreturn<-paste(ldayreturn,"%",sep="")
lpred<-tail(preds,1)
ldaypred<-ifelse(lpred==1,"Long","Short")
trpos<-ifelse(is.na(trpos),0,trpos)
trpos<-ifelse(preds==0,0,trpos)
modename<-signaldata$Model
title<-signaldata$Title
date<-signaldata$SignalDate
equity<-cumprod(trpos+1)
equity<-(equity-1)*100


par(oma=c(0,0,0,0),mar=c(3,2.5,0.5,0.5))
layout(matrix(c(1,2,1,2,3,4),nrow=3,ncol=2, byrow = TRUE),
       widths=c(3,1.3,1.3), heights=c(2.5,2.5,2.5))
# Plot Equity
cex<-0.7
dreturn_title_cex<-1.5
dreturn_number_cex<-2.4

moneyinv<-25
equity_ym<-(equity_ym/100+1)*25
equity<-(equity/100+1)*25

xlab=""
ylab="Equity (in Mio USD)"
eqvals<-c(equity_ym,equity)
minvals<-min(eqvals)
maxvals<-max(eqvals)
plot(c(25,equity),type="l",col="green",lwd=3,xlab=xlab,ylab=ylab,ylim=c(minvals,maxvals),xaxt="n")
m<-length(date)
delta<-8
delta0<-floor(m/delta)
delta0<-ifelse(m<30,1,delta0)
date1<-get_date(date)
date1<-c("Pre-2016-11-29",date1)
datepos<-seq(1,length(date1),delta0)
axis(1,at =datepos,labels = date1[datepos])
points(c(25,equity_ym),type="l",col="gray",lwd=2)
abline(h=seq(-100,100,5),col="gray81",lty=2,lwd=0.5)
mtext(text = "Equity (Mio USD)",side = 2,line=-1.5,cex=0.9)
legend1<-"Strategy"
legend2<-"YM Close"
legend3<-"25 Mio USD"
legend("topleft",legend = c(legend1,legend2,legend3),lty=c(1,1,2),lwd=c(3,2,2),col = c("green","gray","black"),cex=1.2,ncol = 3)
abline(h=25,col="black",lwd=2,lty=2)
# ## Plot Sharpe
# ylab="Sharpe"
# if(length(trpos)<=51)
# {plot.new()
#   box()
#   mtext(text = "Need more than 50 days",side = 3,line=-4,cex=1.1,col="red")
#   mtext(text = "Roll. Sharpe",side = 4,line=-1,cex=cex)}else{
# sharpe_str<-rollapply(trpos,width=50,FUN=sharpe)
# sharpe_ym<-rollapply(rets_ym,width=50,FUN=sharpe)
# sharpevals<-c(sharpe_str,sharpe_ym)
# minsharpevals<-min(sharpevals)
# maxsharpevals<-max(sharpevals)
# plot(sharpe_str,ylab=ylab,xlab=xlab,type="l",col="green",lwd=2.2,ylim=c(minsharpevals,maxsharpevals),xaxt="n")
# points(sharpe_ym,type="l",col="gray",lwd=1.5)
# m<-length(date)
# delta<-3.7
# delta0<-floor(m/delta)
# datepos<-seq(1,length(date),delta0)
# axis(1,at =datepos,labels = get_dates(date)[datepos+50])
# mtext(text = "Roll. Sharpe",side = 4,line=-1,cex=cex)}
# Max Draw Down Plot
maxdraw_str<-report.maxdrawdown_profile(trpos)
maxdraw_ym<-report.maxdrawdown_profile(rets_ym)
maxdrawvals<-c(maxdraw_ym,maxdraw_str)
maxmaxdrawvals<-max(maxdrawvals)
minmaxdrawvals<-min(maxdrawvals)
plot(maxdraw_str,ylab=ylab,xlab=xlab,type="l",col="green",lwd=2.2,ylim=c(minmaxdrawvals,maxmaxdrawvals),xaxt="n")
points(maxdraw_ym,type="l",col="gray",lwd=1.5)
mtext(text = "MDD (in %)",side = 4,line=-1,cex=cex)
m<-length(date)
delta<-3.8
delta0<-floor(m/delta)
delta0<-ifelse(m<30,1,delta0)
datepos<-seq(1,length(date),delta0)
axis(1,at =datepos,labels = get_dates(date)[datepos])
trpos1<-trpos*equity
minval<-min(trpos1,na.rm=TRUE)
maxval<-max(trpos1,na.rm=TRUE)
mval<-max(abs(minval),abs(maxval))
barplot(trpos1,ylim=c(-mval,mval),col="green",axes=T)
box()
mtext(text = "Return (Mio EUR)",side = 2,line=-1.5,cex=0.60)
## Plot Daily Return
plot.new()
#box(which = "plot", lty = "solid")
mtext(text = ldayreturn,line=-6.1-0.4,cex=dreturn_number_cex,col="forestgreen")
str1<-"Last Trade Profit"
mtext(text=str1,line=-1.2-0.4,cex=dreturn_title_cex)
datend<-paste("(",date_end," / Signal:  ",ldaypred,")",sep="")
mtext(text=datend,line=-2.6-0.4,cex=1.0)
#
```

# **Total Equity (in Mio USD)**
| Date |  | Equity|
|:-------------:|:-----------:|:-----------:|
| Start (`r date_begin`)| | `r 25.00`|
| Current (`r date_end`)| | `r formN(tail(equity,1))`

# **PnL**

|     | 10 days|  30 days |Total|YM/Close| 
|------------------------------|:---------------:|:--------:|:--------:|:------:|
| **PnL (in Mio USD)**       |    **`r formN((s10$performance/100)*25)`**|**`r formN(s30$performance*25/100)`** | **`r formN(s0$performance*25/100)`**|`r formN(s0ym$performance*25/100)`|
| **PnL (in %)**       |    **`r formN(s10$performance)`**|**`r formN(s30$performance)`** | **`r formN(s0$performance)`**|`r formN(s0ym$performance)`|

# **Risk and Performance Diagnostics**

|Risk/Cost Metrics      | 10 days|  30 days |Total|YM/Close| 
|------------------------------|:---------------:|:--------:|:--------:|:------:|
| Avg. Trade Profit (in bps) | `r formN(s10$avg_trprofit)`|`r formN(s30$avg_trprofit)`|`r formN(s0$avg_trprofit)`|`r formN(s0ym$avg_trprofit)`|
| Signal Accuracy (in %)                  | `r formN(s10$accr)` |`r formN(s30$accr)`|`r formN(s0$accr)`|`r formN(s0ym$accr)`|
| Annualized Sharpe  Ratio                   | `r formN(s10$sharpe)`  |`r formN(s30$sharpe)`  | `r formN(s0$sharpe)`|`r formN(s0ym$sharpe)`|
| **Max. Drawdown (MDD) (in %)**                | **`r formN(-s10$maxdrwdwn)`** |**`r formN(-s30$maxdrwdwn)`** |**`r formN(-s0$maxdrwdwn)`**|`r formN(-s0ym$maxdrwdwn)`|
| PnL/MDD  | `r formN(s10$pnlmddratio)`|`r formN(s30$pnlmddratio)`|`r formN(s0$pnlmddratio)`|`r formN(s0ym$pnlmddratio)`|
| Round Trip Costs (in bps)| `r formN(1.00)`|`r formN(1.00)` |`r formN(1.00)`|`--`|


# **Trade Diagnostics**

| Long/Short Trades      |       | Win/Loss Trades  || Trade Profits (in bps)  ||
| ------------- |-------------:| -----:|-----:|-----:|----:|
| All     |  `r s0$ntotal`       |All   |    `r s0$ntotal` |  Avg | `r formN(s0$avg_trprofit,d=5)`|
| Long      | `r s0$nlong`|  Win Trades    |   `r s0$nwin` | Max Profit | `r formN(s0$max_wintrade,d=6)`|
| Short | `r s0$nshort` | Loss Trades      |    `r s0$nlos` | Max  Loss | `r formN(-s0$min_losstrade,d=6)`|

[^1]: Left panel shows the equity line. Right panel shows the rolling Sharpe ratio, the rolling maximum drawdown (MDD) and the last trade performance. The lower panel shows the daily strategy returns in basis points. 



