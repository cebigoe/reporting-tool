---
title: "K1 Signal Performance Report"
author: ""
date: "2016-12-01 11:17:50"
output:
  prettydoc::html_pretty:
    theme: cayman
    highlight: github
  html_document:
    fig_caption: yes
    keep_md: yes
    self_contained: Yes

---
 ![](./42cxlogo.png)


### Trading Period
| First Date | Last Date | Trading Days|
|:-------------:|:-----------:|:-----------:|
| 2015-10-15| 2016-11-30| 277|

# **Equity Line**[^1]
<img src="figure/Q4-1.png" title="plot of chunk Q4" alt="plot of chunk Q4" style="display: block; margin: auto;" />

# **Performance Diagnostics**

|Key Metrics      | 10 days|  30 days |Total|YM/Close| 
|------------------------------|:---------------:|:--------:|:--------:|:------:|
| **PnL (in %)**       |    **-0.40**|**03.07** | **57.67**|13.38|
| Avg. Trade Profit (in bps) | -3.99|10.22|16.62|04.75|
| Accuracy (in %)                  | 50.00 |60.00|61.37|54.04|
| Sharpe  Ratio                   | -2.94  |03.02  | 04.43|00.91|
| **Max. Drawdown (MDD) (in %)**                | **00.66** |**01.37** |**02.54**|12.64|
| PnL/MDD  | 00.60|02.24|22.67|01.06|
| Round Trip Costs (in bps)| 01.00|01.00 |01.00|`--`|


# **Trade Diagnostics**

| Long/Short Trades      |       | Win/Loss Trades  || Trade Profits (in bps)  ||
| ------------- |-------------:| -----:|-----:|-----:|----:|
| All     |  277       |All   |    277 |  Avg | 16.62|
| Long      | 152|  Win Trades    |   170 | Max Profit | 249.77|
| Short | 125 | Loss Trades      |    107 | Max  Loss | 153.55|

[^1]: Left panel shows the equity line. Right panel shows the rolling Sharpe ratio, the rolling maximum drawdown (MDD) and the last trade performance. The lower panel shows the daily strategy returns in basis points. 



